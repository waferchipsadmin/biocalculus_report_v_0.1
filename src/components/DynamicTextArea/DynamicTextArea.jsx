import { useEffect, useRef, useState } from "react";

const DynamicTextArea = ({ value: initialValue }) => {
  const [value, setValue] = useState(initialValue);
  const [fontSize, setFontSize] = useState(12.1);
  const ref = useRef();

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  useEffect(() => {
    const { scrollHeight } = ref.current;

    if (scrollHeight > 209) {
      setFontSize(fontSize - 0.2);
    }
  }, [value, fontSize]);

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  return (
    <textarea
      ref={ref}
      type="paragraph"
      defaultValue={value}
      style={{ fontSize: `${fontSize}px` }}
      name="findings"
      onChange={handleChange}
      className="content"></textarea>
  );
};

export default DynamicTextArea;
