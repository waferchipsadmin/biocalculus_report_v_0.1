import "./App.css";
import { Report } from "./components";

function App() {
  return (
    <div>
      <Report />
    </div>
  );
}

export default App;
